# updatesoa.sh

A simple script to update BIND SOA serial numbers.

## Usage

This script is intended to be used in conjunction with an $INCLUDE statement in
the zone file referencing a separate SOA file.  This is especially important if
multiple zones exist on your server.

All that should be necessary to use ``updatesoa.sh`` is to modify the SOA
variable to point to your record or zone file and to ensure that the line
containing the serial number has the string “Serial” as a comment
(such as in the default zones that ship with BIND9 on Debian).

You may need to modify the $SOA path depending on how you have configured your
zone(s).

If successful you will see something like this:

```sh
root@jupiter:/etc/bind# ./updatesoa.sh
Current DNS serial:     2015030807
New DNS serial:         2015030808
SOA updated.
```

Reminder: after this you will need to reload the zone!

Obvious error conditions are trapped in the script but it is probably far from foolproof. 
